class Persona:
    def __init__(self, nombre, apellidos, nacimiento, DNI):
        self._nombre = nombre
        self._apellidos = apellidos
        self._nacimiento = nacimiento
        self._DNI = DNI

    def setNombre(self, nombre):
        self._nombre = nombre
    
    def setApellidos(self, apellidos):
        self._apellidos = apellidos

    def setNacimiento(self, nacimiento):
        self._nacimiento = nacimiento

    def setDNI(self, DNI):
        self._DNI = DNI


    def getNombre(self): #el getter, si luego pongo print(Persona.nombre) no me lo va a enseñar porque 
                         #he puesto _ y es privado, tengo que pner print(Perspna.getNombre()) y ya me 
                         #lo va a enseñar
        return self._nombre
    
    def getApellidos(self):
        return self._apellidos

    def getNacimiento(self):
        return self._nacimiento

    def getDNI(self):
        return self._DNI

    
    def __str__(self):
        return "Paciente: nombre:" + str(self._nombre)+ "; apellidos:"+ str(self._apellidos)+ "; fecha de nacimiento:"+ str(self._nacimiento)+ "; DNI:"+ str(self._DNI)



class Paciente(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, historal_clinico):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self._historal_clinico = historal_clinico


    def ver_historial_clinico(self):
        return "Historial Clínico:", str(self._historal_clinico)

class Medico(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, especialidad, citas):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self._especialidad = especialidad
        self._citas = citas
        citas = []

    def consultar_agenda(self):
        if self._citas == 0:
            return "No hay citas."
        else:
            return "Citas:", str(self._citas)
  
paciente1 = Paciente("Iciar", "Ciprián", "12/06/2005", "45646545J", "Historial de Iciar Ciprián")
print(paciente1)
print(paciente1.ver_historial_clinico())
citas =["Cita 1 - 10:00 AM", "Cita 2 - 02:30 PM"]
medico1 = Medico("Dr. Moreno", "Quintanilla", "04/09/2004", "17092023", "Neurología", citas)
print(medico1)
print(medico1.consultar_agenda())
