import unittest
from codigohospital import Medico
from codigohospital import Paciente
from codigohospital import Persona

class TestClases(unittest.TestCase):

    def test_persona_get_set_nombre(self):
        persona = Persona("Juan", "Gómez", "01/01/1990", "12345678A")
        self.assertEqual(persona.getNombre(), "Juan")
        persona.setNombre("NuevoNombre")
        self.assertEqual(persona.getNombre(), "NuevoNombre")

    def test_paciente_ver_historial_clinico(self):
        paciente = Paciente("Iciar", "Ciprián", "12/06/2005", "45646545J", "Historial de Iciar Ciprián")
        self.assertEqual(paciente.ver_historial_clinico(), ("Historial Clínico:", "Historial de Iciar Ciprián"))

    def test_medico_consultar_agenda(self):
        citas = ["Cita 1 - 10:00 AM", "Cita 2 - 02:30 PM"]
        medico = Medico("Dr. Moreno", "Quintanilla", "04/09/2004", "17092023", "Neurología", citas)
        self.assertEqual(medico.consultar_agenda(), ("Citas:", "['Cita 1 - 10:00 AM', 'Cita 2 - 02:30 PM']"))

if __name__ == '__main__':
    unittest.main()